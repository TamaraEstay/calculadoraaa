/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.calculadora.modelo;

/**
 *
 * @author arnaldoaraya
 */
public class calculadoraServicio {
    
    private static final int PARAMETRO = 100;
    
    public String calculoInteres(int capital, int anios, float tasaInteres){
        float interes = tasaInteres/PARAMETRO;
        float total = capital*interes*anios;
        return Float.toString(total);
    }
}
